# Ejecución de otras Aplicaciones desde Java

## Introducción

Java nos permite llamar a otras aplicaciones desde nuestro código.  Para ello nos da distintas herramientas:

* Runtime.getRuntime().exec().
* ProcessBuilder API.

En el repositorio se encuentra un fichero llamado __dummy.jar__, que cuando lo ejecutamos produce la siguiente salida:

```bash
$ java -jar dummy.jar
Hello World!
```

## Runtime.getRuntime().exec()

Llamada al fichero __dummy.jar__ utilizando Runtime:

```java
String line = "java -jar dummy.jar";
CommandLine commandLine = CommandLine.parse(line);
DefaultExecutor executor = new DefaultExecutor();
executor.setExitValue(1);
int exitValue = executor.execute(commandLine);
```

## ProcessBuilder API

Llamada al fichero __dummy.jar__ utilizando ProcessBuilder:

```java
ProcessBuilder pb;
try {
     pb = new ProcessBuilder("java", "-jar", "D:\\dummy.jar");           
     pb.inheritIO();
     pb.start();
} catch (Exception e) {
     e.printStackTrace();
}
```

El método __inheritIO__ nos permite capturar la salida del script y mostrarla por pantalla.

Si queremos volcar la salida del script a un fichero debemos sustituir el método __inheritIO__ por __redirectOutput__:

```java
ProcessBuilder pb;
try {
     pb = new ProcessBuilder("java", "-jar", "D:\\dummy.jar");           
     pb.redirectOutput(new File("D:\\dummy.out"));
     pb.start();
} catch (Exception e) {
     e.printStackTrace();
}
```

> __Nota__: Tener en cuenta en este ejemplo que el fichero dummy.jar se encuentra en la ruta D:. Tendremos que modificar el path del fichero para que apunte a la ruta donde hayamos guardado el fichero. 